import React, { Fragment } from 'react';
import {
  Header, Loader, Message,
} from 'semantic-ui-react';
import { loadConcerts, deleteConcert } from '../../services/localstorage';
import OfflineConcertTable from './OfflineConcertTable';
import WarningMessage from './WarningMessage';
import { sortConcertByDate } from '../../common/sortHelper';

class OfflineConcert extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      concerts: [],
      loading: true,
    };
  }

  componentDidMount() {
    loadConcerts()
      .then((concerts) => {
        this.setState({
          concerts: concerts.sort(sortConcertByDate),
          loading: false,
        });
      })
      .catch((err) => {
        this.displayError(`Offline verfügbare Konzerte konnten nicht geladen werden: ${err}`);
      });
  }

  onRemove = (concert) => {
    deleteConcert(concert);
    this.setState(state => ({ concerts: state.concerts.filter(c => c.id !== concert.id) }));
  };

  displayError(err) {
    this.setState({
      error: err,
      loading: false,
    });
  }

  render() {
    const showWarning = !this.state.concerts || this.state.concerts.length < 1;
    const showError = !!this.state.error;
    return (
      <Fragment>
        <Header as="h1">
          Offline verfügbare Konzerte
        </Header>
        { // eslint-disable-next-line no-nested-ternary
          this.state.loading
            ? <Loader active={this.state.loading} inline="centered" />
            // eslint-disable-next-line no-nested-ternary
            : (showWarning
              ? <WarningMessage />
              : showError
                ? <Message header="Fehler!" content={this.state.error} error />
                : (
                  <OfflineConcertTable
                    concerts={this.state.concerts}
                    onRemove={this.onRemove}
                    loading={this.state.loading}
                  />
                )
            )}
      </Fragment>
    );
  }
}

export default OfflineConcert;
