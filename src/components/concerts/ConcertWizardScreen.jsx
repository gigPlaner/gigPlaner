import React, { Component, Fragment } from 'react';
import {
  Icon, Step, Button, Divider, Dimmer, Loader, Message,
} from 'semantic-ui-react';
import ReactRouterPropTypes from 'react-router-prop-types';
import NewConcertData from './NewConcertData';
import SongSelection from './SongSelection';
import ConcertAnalyse from '../analyzeConcert/ConcertAnalyse';
import * as firestore from '../../services/firestore';
import { convertTimeStringToSeconds, convertSecondsToTimeString, addTimeColon } from '../../common/formHelper';
import { FormInputRules, FormInputValidator } from '../../common/formInputValidator';
import { sortSongByName } from '../../common/sortHelper';

const ScreenEnum = (() => {
  const screens = { NewConcert: 1, SongSelection: 2, ConcertAnalyse: 3 };
  Object.freeze(screens);
  return screens;
})();

const TIME_BREAK = 20; // Time between the Songs in seconds

export default class ConcertWizardScreen extends Component {
  formValidation = {
    date: [FormInputRules.isRequired(), FormInputRules.isDate()],
    name: [FormInputRules.isRequired()],
    location: [FormInputRules.isRequired()],
    maxDuration: [FormInputRules.isRequired(), FormInputRules.isTime()],
  };

  state = {
    activeStep: ScreenEnum.NewConcert,
    loading: true,
    concert: {
      id: '',
      date: '',
      name: '',
      location: '',
      maxDuration: '',
      description: '',
      actualDuration: 0,
    },
    isConcertValid: false,
    songRepository: [],
    selectedSongs: [],
    error: '',
    formErrors: {},
  };

  componentDidMount() {
    const concertId = this.props.match.params.id;
    this.loadConcert(concertId);
  }

  switchStep = (id) => {
    this.setState({
      activeStep: id,
    });
  };

  storeConcert = () => {
    const concert = {
      id: this.state.concert.id,
      date: this.state.concert.date,
      name: this.state.concert.name,
      location: this.state.concert.location,
      maxDuration: convertTimeStringToSeconds(this.state.concert.maxDuration),
      description: this.state.concert.description,
      actualDuration: this.state.concert.actualDuration,
      songs: this.state.selectedSongs.map(s => s.id),
    };
    if (concert.id) {
      this.updateConcert(concert);
    } else {
      this.createConcert(concert);
    }
  };

  createConcert= (concert) => {
    firestore.storeConcert(concert)
      .then(this.props.history.push('/concerts'))
      .catch((err) => {
        this.displayError('Fehler beim Erstellen vom Konzert!');
        firestore.logError(err);
      });
  };

  updateConcert = (concert) => {
    firestore.updateConcert(concert)
      .then(this.props.history.push('/concerts'))
      .catch((err) => {
        this.displayError('Fehler beim Speichern vom Konzert!');
        firestore.logError(err);
      });
  };

  loadConcert = (concertId) => {
    if (concertId) {
      firestore.getConcert(concertId)
        .then((concertData) => {
          if (concertData) {
            const concert = {
              ...concertData,
              maxDuration: convertSecondsToTimeString(concertData.maxDuration),
            };
            this.setState({
              concert,
              isConcertValid: this.validateConcert(concert),
            });
            return concert;
          }
          this.displayError(`Fehler beim Laden vom Konzert! Konzert mit ID ${this.props.match.params.id} existiert nicht.`);
          return null;
        })
        .then((concert) => {
          this.loadSongRepository(concert);
        })
        .catch((err) => {
          this.displayError('Fehler beim Laden vom Konzert!');
          firestore.logError(err);
        });
    } else {
      this.loadSongRepository();
    }
  };

  loadSongRepository = concert => firestore.loadSongsWithGenres()
    .then((songs) => {
      this.createSongRepo(concert, songs);
    });

  createSongRepo = (concert, songs) => {
    const songRepository = Object.values(songs);
    const selectedSongs = [];
    if (concert) {
      concert.songs.forEach((songSelectedId) => {
        const newSong = songRepository.find(song => song.id === songSelectedId);
        if (newSong !== undefined) {
          selectedSongs.push(newSong);
          newSong.selected = true;
        }
      });
    }

    this.setState(() => ({
      loading: false,
      songRepository: songRepository.sort(sortSongByName),
      selectedSongs,
    }));
  };

  validateConcert = (concert) => {
    let formValid = true; // assume form is valid

    Object.keys(this.formValidation).forEach(
      (inputField) => {
        const rules = this.formValidation[inputField];
        const value = concert[inputField];
        if (!FormInputValidator.validate(value, rules)) {
          formValid = false;
        }
      },
    );
    return formValid;
  };

  handleConcertInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    let value = target.value;
    if (name === 'maxDuration') {
      value = addTimeColon(this.state.concert.maxDuration, value);
    }
    this.setState((oldState) => {
      const concert = { ...oldState.concert, [name]: value };
      const isConcertValid = this.validateConcert(concert);
      return ({
        concert,
        isConcertValid,
      });
    });
  };

  validateInputField = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    const rules = this.formValidation[name];
    const isFieldValid = FormInputValidator.validate(value, rules);
    this.markInputFieldError(name, isFieldValid);
  };

  markInputFieldError = (input, isFieldValid) => {
    this.setState(prevState => ({
      formErrors: {
        ...prevState.formErrors,
        [input]: !isFieldValid,
      },
    }));
  };

  addSong = (song) => {
    this.setState((oldState) => {
      const selectedSongs = [...oldState.selectedSongs, song];
      const actualDuration = this.calculateActualTime(selectedSongs);
      const songRepository = oldState.songRepository.map((s) => {
        if (s.id === song.id) {
          return { ...s, selected: true };
        }
        return s;
      });

      return ({
        concert: { ...oldState.concert, actualDuration },
        selectedSongs,
        songRepository,
      });
    });
  };

  removeSong = (song) => {
    this.setState((oldState) => {
      const selectedSongs = oldState.selectedSongs.filter(s => s.id !== song.id);
      const actualDuration = this.calculateActualTime(selectedSongs);
      const songRepository = oldState.songRepository.map((s) => {
        if (s.id === song.id) {
          return { ...s, selected: false };
        }
        return s;
      });

      return ({
        concert: { ...oldState.concert, actualDuration },
        selectedSongs,
        songRepository,
      });
    });
  };

  moveSong = (sourceIndex, destinationIndex) => {
    this.setState((oldState) => {
      const selectedSongs = Array.from(oldState.selectedSongs);
      const temp = selectedSongs.splice(sourceIndex, 1);
      selectedSongs.splice(destinationIndex, 0, temp[0]);
      return ({
        selectedSongs,
      });
    });
  };

  calculateActualTime = (selectedSongs) => {
    if (!selectedSongs || selectedSongs.length < 1) {
      return 0;
    }
    return selectedSongs
      .reduce((totalDuration, song) => totalDuration + song.duration + TIME_BREAK, -TIME_BREAK);
  };

  displayError = (error) => {
    this.setState({
      error,
      loading: false,
    });
  };

  render() {
    return (
      <Fragment>
        <Dimmer inverted active={this.state.loading}>
          <Loader />
        </Dimmer>
        <Step.Group widths={4} size="tiny">
          <Step
            onClick={() => this.switchStep(ScreenEnum.NewConcert)}
            active={this.state.activeStep === ScreenEnum.NewConcert}
          >
            <Icon name="magic" />
            <Step.Content>
              <Step.Title>Konzert</Step.Title>
            </Step.Content>
          </Step>
          <Step
            onClick={() => this.switchStep(ScreenEnum.SongSelection)}
            active={this.state.activeStep === ScreenEnum.SongSelection}
            disabled={!this.state.isConcertValid}
          >
            <Icon name="music" />
            <Step.Content>
              <Step.Title>Songauswahl</Step.Title>
            </Step.Content>
          </Step>
          <Step
            onClick={() => this.switchStep(ScreenEnum.ConcertAnalyse)}
            active={this.state.activeStep === ScreenEnum.ConcertAnalyse}
            disabled={!this.state.isConcertValid}
          >
            <Icon name="info" />
            <Step.Content>
              <Step.Title>Analyse</Step.Title>
            </Step.Content>
          </Step>
          <Step
            disabled={!this.state.isConcertValid}
            onClick={this.storeConcert}
          >
            <Icon name="save" />
            <Step.Content>
              <Step.Title>Abschliessen</Step.Title>
            </Step.Content>
          </Step>
        </Step.Group>

        {!this.state.loading
        && ((activeStep) => {
          switch (activeStep) {
          case ScreenEnum.NewConcert:
            return (
              <NewConcertData
                concert={this.state.concert}
                formErrors={this.state.formErrors}
                handleInputChange={this.handleConcertInputChange}
                validateInputField={this.validateInputField}
              />
            );
          case ScreenEnum.SongSelection:
            return (
              <SongSelection
                songRepository={this.state.songRepository}
                selectedSongs={this.state.selectedSongs}
                maxDuration={convertTimeStringToSeconds(this.state.concert.maxDuration)}
                actualDuration={this.state.concert.actualDuration}
                addSong={this.addSong}
                removeSong={this.removeSong}
                moveSong={this.moveSong}
              />
            );
          case ScreenEnum.ConcertAnalyse:
            return (
              <ConcertAnalyse
                songs={this.state.selectedSongs}
                concert={this.state.concert}
              />
            );
          default:
            return (
              <NewConcertData
                concert={this.state.concert}
                formErrors={this.state.formErrors}
                handleInputChange={this.handleConcertInputChange}
                validateInputField={this.validateInputField}
              />
            );
          }
        })(this.state.activeStep)}

        {this.state.error
          && (
            <Message
              error
              header="Fehler"
              content={this.state.error}
            />
          )
        }
        <Divider />
        <Button negative onClick={() => (this.props.history.push('/concerts'))}>Abbrechen</Button>
      </Fragment>);
  }
}

ConcertWizardScreen.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
};
