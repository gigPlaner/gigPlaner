import { Header } from 'semantic-ui-react';
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { ConcertFormType, SongType } from '../../constants/types';
import { convertSecondsToTimeString } from '../../common/formHelper';

const ConcertAnalyseInformation = (props) => {
  const { songs, concert } = props;
  return (
    <Fragment>
      <Header as="h3">
      Informationen
      </Header>
      <p>
        {`Anzahl Songs:  ${songs.length}`}
      </p>
      <p>
        {`Maximale Konzertlänge:  ${concert.maxDuration}`}
      </p>
      <p>
        {`Aktuelle Konzertlänge:  ${convertSecondsToTimeString(concert.actualDuration)}`}
      </p>
    </Fragment>);
};

export default ConcertAnalyseInformation;


ConcertAnalyseInformation.propTypes = {
  songs: PropTypes.arrayOf(SongType).isRequired,
  concert: ConcertFormType.isRequired,
};
