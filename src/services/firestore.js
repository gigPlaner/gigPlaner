import firebase from 'firebase/app';
import 'firebase/firestore';

// collection names
const GENRE_COL = 'genres';
const SONG_COL = 'songs';
const CONCERT_COL = 'concerts';
const ERROR_COL = 'errors';

const config = {
  apiKey: 'AIzaSyBqwHXTvHj2coyBnjCUHck2aKhMRg3b00A',
  authDomain: 'gig-planer.firebaseapp.com',
  databaseURL: 'https://gig-planer.firebaseio.com',
  messagingSenderId: '891109215848',
  projectId: 'gig-planer',
  storageBucket: 'gig-planer.appspot.com',
};
firebase.initializeApp(config);

const firestore = firebase.firestore();
const settings = {
  timestampsInSnapshots: true,
};
firestore.settings(settings);

// enable firestore offline functionality
// firestore.enablePersistence();

// -----------------
// CONCERTS
// -----------------

function extractSongIds(songReference) {
  if (songReference) {
    return songReference.map(s => s.id);
  }
  return [];
}

function getSongRef(songId) {
  // create the document reference for the song
  return firestore.collection(SONG_COL).doc(songId);
}

function createConcertObject(doc) {
  if (!doc.exists) {
    return null;
  }
  return {
    id: doc.id,
    name: doc.data().name,
    date: doc.data().date,
    location: doc.data().location,
    maxDuration: doc.data().maxDuration,
    actualDuration: doc.data().actualDuration,
    description: doc.data().description,
    songs: extractSongIds(doc.data().songs),
  };
}

function createConcertDatabaseObject(concert) {
  return {
    name: concert.name,
    date: concert.date,
    location: concert.location,
    maxDuration: concert.maxDuration,
    actualDuration: concert.actualDuration,
    description: concert.description,
    songs: concert.songs.map(id => getSongRef(id)),
  };
}

export async function loadConcerts() {
  const query = await firestore.collection(CONCERT_COL).get();

  const concertList = [];
  query.forEach((doc) => {
    concertList.push(createConcertObject(doc));
  });
  return concertList;
}

export function addConcertChangeListener(callbackFunction) {
  firestore.collection(CONCERT_COL)
    .onSnapshot((querySnapshot) => {
      const concertList = [];
      querySnapshot.forEach((doc) => {
        concertList.push(createConcertObject(doc));
      });
      callbackFunction(concertList);
    });
}

export async function getConcert(id) {
  const doc = await firestore.collection(CONCERT_COL)
    .doc(id)
    .get();

  return createConcertObject(doc);
}

export function storeConcert(concert) {
  return firestore.collection(CONCERT_COL)
    .add(createConcertDatabaseObject(concert));
}

export function updateConcert(concert) {
  return firestore.collection(CONCERT_COL)
    .doc(concert.id)
    .update(createConcertDatabaseObject(concert));
}

// -----------------
// GENRES
// -----------------

function createGenreObject(doc) {
  if (!doc.exists) {
    return null;
  }
  return {
    id: doc.id,
    genre: doc.data().genre,
    color: doc.data().color,
  };
}

function createGenreDatabaseObject(genre) {
  return {
    genre: genre.genre,
    color: genre.color,
  };
}

export async function loadGenres() {
  const query = await firestore.collection(GENRE_COL).get();

  const genreList = [];
  query.forEach((doc) => {
    genreList.push(createGenreObject(doc));
  });
  return genreList;
}

export function addGenreChangeListener(callbackFunction) {
  firestore.collection(GENRE_COL)
    .onSnapshot((querySnapshot) => {
      const genreList = [];
      querySnapshot.forEach((doc) => {
        genreList.push(createGenreObject(doc));
      });
      callbackFunction(genreList);
    });
}

export function storeGenre(genre) {
  return firestore.collection(GENRE_COL)
    .add(createGenreDatabaseObject(genre));
}

export function updateGenre(genre) {
  return firestore.collection(GENRE_COL)
    .doc(genre.id)
    .update(createGenreDatabaseObject(genre));
}

export async function getGenre(id) {
  const doc = await firestore.collection(GENRE_COL)
    .doc(id)
    .get();

  return createGenreObject(doc);
}

// -----------------
// SONGS
// -----------------
function getGenreRef(genreId) {
  // create the document reference for the genre
  return firestore.collection(GENRE_COL).doc(genreId);
}

function createSongObject(doc) {
  if (!doc.exists) {
    return null;
  }
  return {
    id: doc.id,
    title: doc.data().title,
    genre: doc.data().genre.id,
    bpm: doc.data().bpm,
    duration: doc.data().duration,
    description: doc.data().description,
    fileURL: doc.data().fileURL,
    active: doc.data().active,
  };
}

function createSongDatabaseObject(song) {
  return {
    title: song.title,
    genre: getGenreRef(song.genre),
    bpm: song.bpm,
    duration: song.duration,
    description: song.description,
    fileURL: song.fileURL,
    active: song.active,
  };
}

export function storeSong(song) {
  // store the new song
  return firestore.collection(SONG_COL)
    .add(createSongDatabaseObject(song));
}

export function updateSong(song) {
  // update the song
  return firestore.collection(SONG_COL)
    .doc(song.id)
    .update(createSongDatabaseObject(song));
}

export async function getSong(id) {
  const doc = await firestore.collection(SONG_COL)
    .doc(id)
    .get();

  return createSongObject(doc);
}

export async function loadSongs() {
  const query = await firestore.collection(SONG_COL).get();

  const songList = [];
  query.forEach((doc) => {
    songList.push(createSongObject(doc));
  });
  return songList;
}

export function addSongChangeListener(callbackFunction) {
  firestore.collection(SONG_COL)
    .onSnapshot((querySnapshot) => {
      const songList = [];
      querySnapshot.forEach((doc) => {
        songList.push(createSongObject(doc));
      });
      callbackFunction(songList);
    });
}

export async function loadSongsWithGenres() {
  const songPromise = firestore.collection(SONG_COL).get();
  const genrePromise = loadGenres();

  const promises = await Promise.all([songPromise, genrePromise]);
  const songDocuments = promises[0];
  const genresMap = promises[1].reduce((genres, genre) => ({
    ...genres, [genre.id]: { ...genre },
  }), {});

  const songs = [];
  songDocuments.forEach((doc) => {
    const genre = doc.data().genre ? genresMap[doc.data().genre.id] : null;
    songs.push({
      ...createSongObject(doc),
      selected: false,
      genre,
    });
  });
  return songs;
}

export async function getConcertWithSongs(id) {
  const concert = await getConcert(id);
  if (!concert) {
    return null;
  }
  const songPromises = [];
  concert.songs.forEach((songId) => {
    songPromises.push(getSong(songId));
  });

  const songs = await Promise.all(songPromises);
  return {
    ...concert,
    songs: songs.filter(s => s), // filter null values
  };
}

// Error
export function logError(errorMessage, componentStack) {
  try {
    return firestore.collection(ERROR_COL)
      .add({
        timestamp: firebase.firestore.Timestamp.now(),
        error: errorMessage,
        componentStack: componentStack || '',
      })
      .then(doc => doc.id);
  } catch (err) {
    return null;
  }
}
