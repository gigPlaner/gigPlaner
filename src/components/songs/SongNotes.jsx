import React from 'react';
import {
  Modal, Button, Header, Icon, Image, Dimmer, Loader,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';

class SongNotes extends React.Component {
  state = {
    modalOpen: false,
    loading: true,
  };

  // without state check the Image onLoad throws a fancy error,
  // workaround: only update state if needed
  handleImageLoaded = () => {
    if (this.state.loading) {
      this.setState({ loading: false });
    }
  };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    return (
      <Modal
        trigger={<Button icon onClick={this.handleOpen}><Icon name="music" /></Button>}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        closeIcon
      >
        <Header>
          {this.props.song}
        </Header>
        <Modal.Content>
          <Dimmer inverted active={this.state.loading}>
            <Loader />
          </Dimmer>
          <Image centered src={this.props.notes} onLoad={this.handleImageLoaded} />
        </Modal.Content>
      </Modal>
    );
  }
}

SongNotes.propTypes = {
  notes: PropTypes.string.isRequired,
  song: PropTypes.string.isRequired,
};

export default SongNotes;
