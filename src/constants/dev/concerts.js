const CONCERTS = [
  {
    id: 1,
    name: 'A Konzert 1',
    date: '01.08.2018',
    location: 'Location 1',
    notes: 'Notes 1',
  },
  {
    id: 2,
    name: 'A Konzert 2',
    date: '02.08.2018',
    location: 'Location 2',
    notes: 'Notes 2',
  },
  {
    id: 3,
    name: 'B Konzert',
    date: '03.08.2018',
    location: 'Location 3',
    notes: 'Notes 3',
  },
  {
    id: 4,
    name: 'B Konzert',
    date: '04.08.2018',
    location: 'Location 4',
    notes: 'Notes 4',
  },
  {
    id: 5,
    name: 'B Konzert',
    date: '05.08.2018',
    location: 'Location 5',
    notes: 'Notes 5',
  },
  {
    id: 6,
    name: 'B Konzert',
    date: '06.08.2018',
    location: 'Location 6',
    notes: 'Notes 6',
  },
];

export default CONCERTS;
