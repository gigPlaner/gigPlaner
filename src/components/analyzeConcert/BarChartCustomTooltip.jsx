import React from 'react';
import PropTypes from 'prop-types';
import { convertSecondsToShortTimeString } from '../../common/formHelper';

const BarChartCustomTooltip = (props) => {
  const {
    active, payload, label, labelName, formatLabelAsTimeString,
  } = props;

  if (active) {
    const firstPayload = payload[0];
    return (
      <div style={{ backgroundColor: 'rgb(255, 255, 255)', padding: '10px', border: '1px solid grey' }}>
        <p>{label}</p>
        <span>
          {labelName}
          {': '}
          {formatLabelAsTimeString
            ? convertSecondsToShortTimeString(firstPayload.value) : firstPayload.value}
        </span>
        <br />
        <span>
          {'Genre: '}
          {firstPayload.payload.genre.genre}
        </span>
      </div>
    );
  }
  return (<div />);
};

BarChartCustomTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.arrayOf(PropTypes.shape({ value: PropTypes.number })),
  label: PropTypes.string,
  labelName: PropTypes.string.isRequired,
  formatLabelAsTimeString: PropTypes.bool,
};

BarChartCustomTooltip.defaultProps = {
  payload: [{ value: null, genre: { genre: '' } }],
  label: '',
  active: false,
  formatLabelAsTimeString: false,
};

export default BarChartCustomTooltip;
