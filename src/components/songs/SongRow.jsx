import React from 'react';
import {
  Table, Icon, Button, Checkbox, Label,
} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import { GenreStoreType, SongType } from '../../constants/types';
import SongNotes from './SongNotes';
import { convertSecondsToTimeString } from '../../common/formHelper';
import connectToStore from '../../common/storeUtil';
import { GenreStore } from '../../common/Store';

const SongRow = (props) => {
  const genre = props.genrestore.state.genres.find(g => g.id === props.song.genre);
  return (
    <Table.Row>
      <Table.Cell>
        {props.song.title}
      </Table.Cell>
      <Table.Cell>
        <Label style={{ background: genre && genre.color }} horizontal />
        {genre && genre.genre}
      </Table.Cell>
      <Table.Cell>
        {convertSecondsToTimeString(props.song.duration)}
      </Table.Cell>
      <Table.Cell>
        {props.song.bpm}
      </Table.Cell>
      <Table.Cell>
        <Checkbox checked={props.song.active} disabled />
      </Table.Cell>
      <Table.Cell>
        <NavLink to={`/songs/edit/${props.song.id}`}>
          <Button icon>
            <Icon name="edit" />
          </Button>
        </NavLink>
        <SongNotes notes={props.song.fileURL} song={props.song.title} />
      </Table.Cell>
    </Table.Row>
  );
};

SongRow.propTypes = {
  song: SongType.isRequired,
  genrestore: GenreStoreType.isRequired,
};

export default connectToStore({ genrestore: GenreStore })(SongRow);
