const TIME_REGEX = RegExp('^(\\d{2}:\\d{2}:\\d{2}.)$');
const TIME_WITHOUT_COLON_REGEX = RegExp('^(\\d{2}:\\d{2})$|^(\\d{2})$');
const DIGIT_COLON_REGEX = RegExp('(^(\\d){0,2}$)|(^(\\d){0,2}(\\d|:)+$)');

export function convertTimeStringToSeconds(time) {
  if (time === undefined || time === null || time === '') {
    return '';
  }
  const t = time.split(':');
  return (+t[0]) * 60 * 60 + (+t[1]) * 60 + (+t[2]);
}

// hh:mm:ss
export function convertSecondsToTimeString(seconds) {
  if (seconds === undefined || seconds === null || seconds === '') {
    return '';
  }
  return new Date(seconds * 1000).toISOString().substr(11, 8);
}
// mm:ss (<3600s => hh:mm:ss)
export function convertSecondsToShortTimeString(seconds) {
  if (seconds === undefined || seconds === null || seconds === '') {
    return '';
  }

  if (seconds >= 3600) {
    return convertSecondsToTimeString(seconds);
  }

  return new Date(seconds * 1000).toISOString().substr(14, 5);
}

export function toLocaleDateString(date) {
  if (date === undefined || date === null || date === '') {
    return '';
  }
  return new Date(date).toLocaleDateString('de-CH', { year: 'numeric', month: '2-digit', day: '2-digit' });
}

export function addTimeColon(oldValue, newValue) {
  if (TIME_REGEX.test(newValue) || !DIGIT_COLON_REGEX.test(newValue)) {
    return oldValue;
  }

  const newLength = newValue ? newValue.length : 0;
  const oldLength = oldValue ? oldValue.length : 0;

  if (newLength > 8) {
    return oldValue;
  }

  if (TIME_WITHOUT_COLON_REGEX.test(newValue) && newLength > oldLength) {
    return `${newValue}:`;
  }
  return newValue;
}
