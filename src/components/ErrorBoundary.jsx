import React, { Component } from 'react';
import { Message } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { logError } from '../services/firestore';

export default class ErrorBoundary extends Component {
  state = {
    hasError: false,
    errorId: '',
  };

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    logError(error.stack, info.componentStack)
      .then(id => this.setState({ errorId: id }));
  }

  render() {
    if (this.state.hasError) {
      return (
        <Message negative>
          <Message.Header>Ein Fehler ist aufgetreten</Message.Header>
          <p>
            {`Error-Id: ${this.state.errorId}`}
          </p>
        </Message>
      );
    }
    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
};
