import React from 'react';
import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import SongRow from './SongRow';
import { SongType } from '../../constants/types';
import withLoader from '../LoadingComponent';

const SongTable = props => (
  <Table celled padded>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          Titel
        </Table.HeaderCell>
        <Table.HeaderCell>
          Genre
        </Table.HeaderCell>
        <Table.HeaderCell>
          Dauer
        </Table.HeaderCell>
        <Table.HeaderCell>
          BPM
        </Table.HeaderCell>
        <Table.HeaderCell>
          Aktiv
        </Table.HeaderCell>
        <Table.HeaderCell>
          Aktionen
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {props.songs.map(s => (
        <SongRow song={s} key={s.id} />
      ))}
    </Table.Body>
  </Table>
);

SongTable.propTypes = {
  songs: PropTypes.arrayOf(SongType).isRequired,
};

export default withLoader(SongTable);
