import React, { Fragment } from 'react';
import {
  Input, Form, Checkbox, Button, Dropdown, TextArea, Divider, Header, Message, Dimmer, Loader,
} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import * as firestore from '../../services/firestore';
import FileInput from '../input/FileInput';
import { FormInputRules, FormInputValidator } from '../../common/formInputValidator';
import { convertTimeStringToSeconds, convertSecondsToTimeString, addTimeColon } from '../../common/formHelper';
import connectToStore from '../../common/storeUtil';
import { GenreStore, SongStore } from '../../common/Store';
import { GenreStoreType, SongStoreType } from '../../constants/types';

class SongForm extends React.Component {
  formValidation = {
    title: [FormInputRules.isRequired()],
    genre: [FormInputRules.isRequired()],
    duration: [FormInputRules.isRequired(), FormInputRules.isTime()],
    bpm: [FormInputRules.isRequired(), FormInputRules.isNumber()],
    fileURL: [FormInputRules.isRequired()],
  };

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      genre: '',
      bpm: '',
      duration: '',
      description: '',
      fileURL: '',
      active: true,
      genreOptions: [],
      formStoring: false,
      formError: '',
      formErrors: {},
    };
  }

  async componentDidMount() {
    // load the list of available genres
    await this.props.genrestore.fetchGenre();
    // reformat the array of genres, so our Select component understands it
    if (this.props.genrestore.state.genres) {
      const reformatted = this.props.genrestore.state.genres.map(elem => ({
        key: elem.id,
        text: elem.genre,
        value: elem.id,
      }));
      this.setState({ genreOptions: reformatted });
    }

    if (this.isEditMode()) {
      const song = await this.props.songstore.getSong(this.props.match.params.id);
      if (song) {
        this.setState({
          ...song,
          duration: convertSecondsToTimeString(song.duration),
        });
      } else {
        this.displayError(`Song konnte nicht geladen werden. Song mit ID ${this.props.match.params.id} existiert nicht.`);
      }
    }
  }

  isEditMode = () => this.props.match.params.id !== undefined;

  markInputFieldError = (input, isFieldValid) => {
    this.setState(prevState => ({
      formErrors: {
        ...prevState.formErrors,
        [input]: !isFieldValid,
      },
    }));
  };

  isFormValid = () => {
    let formValid = true; // assume form is valid

    Object.keys(this.formValidation).forEach(
      (inputField) => {
        const rules = this.formValidation[inputField];
        const value = this.state[inputField];
        const isFieldValid = FormInputValidator.validate(value, rules);
        formValid = formValid && isFieldValid;
        this.markInputFieldError(inputField, isFieldValid);
      },
    );

    if (!formValid) {
      this.displayError('Formular unvollständig: Nicht alle erforderlichen Felder sind ausgefüllt.');
    }
    return formValid;
  };

  displayError = (err) => {
    this.setState({
      formError: err,
      formStoring: false,
    });
  };

  handleSubmit = async () => {
    // display "loading" screen
    this.setState({
      formStoring: true,
    });

    if (!this.isFormValid()) {
      return;
    }

    const song = {
      id: this.state.id,
      title: this.state.title,
      description: this.state.description,
      genre: this.state.genre,
      duration: convertTimeStringToSeconds(this.state.duration),
      bpm: parseInt(this.state.bpm, 10),
      active: this.state.active,
      fileURL: this.state.fileURL,
    };

    if (this.isEditMode()) {
      try {
      // update
        await firestore.updateSong(song);
        // redirect back to /songs
        this.props.history.push('/songs');
      } catch (error) {
        this.displayError('Song konnte nicht aktualisiert werden.');
        firestore.logError(error);
      }
    } else {
      try {
        // add new song
        await firestore.storeSong(song);
        // redirect back to /songs
        this.props.history.push('/songs');
      } catch (error) {
        this.displayError('Song konnte nicht gespeichert werden.');
        firestore.logError(error);
      }
    }
  };

  // handels changes in all input fields
  handleInputChange = (event) => {
    const target = event.target;
    let value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    if (name === 'duration') {
      value = addTimeColon(this.state.duration, value);
    }

    this.setState({ [name]: value });
  };

  validateInputField = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    const rules = this.formValidation[name];
    const isFieldValid = FormInputValidator.validate(value, rules);
    this.markInputFieldError(name, isFieldValid);
  };

  // handles selects in the dropdown
  handleSelect = (event, data) => {
    this.setState({ genre: data.value });
  };

  // handles file uploads
  handleUpload = (url) => {
    this.setState({ fileURL: url });
  };

  render() {
    const { error: songError, initialized: songInitialized } = this.props.songstore.state;
    const { error: genreError, initialized: genreInitialized } = this.props.genrestore.state;
    const hasErrors = !!songError || !!genreError || !!this.state.formError;
    const loading = (!genreInitialized || !songInitialized) && !hasErrors;

    return (
      <Fragment>
        <Header as="h1">
          {this.isEditMode() ? 'Song bearbeiten' : 'Neuer Song erfassen'}
        </Header>
        <Dimmer inverted active={loading || this.state.formStoring}>
          <Loader />
        </Dimmer>
        {(!loading || !this.isEditMode())
        && (
          <Form
            noValidate // tell the browser to not validate the form
            autoComplete="off"
            error={hasErrors}
          >
            <Form.Field
              label="Songtitel"
              id="title"
              name="title"
              required
              control={Input}
              autoFocus
              value={this.state.title}
              onChange={this.handleInputChange}
              error={this.state.formErrors.title}
            />
            <Form.Field
              label="Genre"
              id="genre"
              name="genre"
              required
              control={Dropdown}
              fluid
              search
              selection
              options={this.state.genreOptions}
              onChange={this.handleSelect}
              value={this.state.genre}
              error={this.state.formErrors.genre}
            />
            <Form.Field
              label="Dauer"
              id="duration"
              name="duration"
              required
              control={Input}
              type="text"
              placeholder="hh:mm:ss"
              onBlur={this.validateInputField}
              onChange={this.handleInputChange}
              value={this.state.duration}
              error={this.state.formErrors.duration}
            />
            <Form.Field
              label="BPM"
              id="bpm"
              name="bpm"
              required
              control={Input}
              type="number"
              onChange={this.handleInputChange}
              value={this.state.bpm}
              error={this.state.formErrors.bpm}
            />
            <Form.Field
              label="Beschreibung"
              id="description"
              name="description"
              control={TextArea}
              type="text"
              rows="4"
              onChange={this.handleInputChange}
              value={this.state.description}
            />
            <Form.Field
              label="Notenblatt"
              id="fileURL"
              name="fileURL"
              required
              control={FileInput}
              value={this.state.fileURL}
              uploadSuccess={this.handleUpload}
              error={this.state.formErrors.fileURL}
            />
            <Form.Field
              label="Aktiv"
              id="active"
              name="active"
              control={Checkbox}
              checked={this.state.active}
              onChange={this.handleInputChange}
            />
            <Message
              error
              header="Fehler"
              list={[this.state.formError, genreError, songError]}
            />
            <Divider />
            <Button onClick={this.handleSubmit}>
            Speichern
            </Button>
            <NavLink to="/songs">
              <Button
                negative
              >
              Abbrechen
              </Button>
            </NavLink>
          </Form>
        )}
      </Fragment>
    );
  }
}

SongForm.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
  songstore: SongStoreType.isRequired,
  genrestore: GenreStoreType.isRequired,
};

export default connectToStore({ songstore: SongStore, genrestore: GenreStore })(SongForm);
