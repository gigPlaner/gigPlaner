import { Header } from 'semantic-ui-react';
import {
  Cell,
  Pie, PieChart, ResponsiveContainer, Tooltip, Legend,
} from 'recharts';
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const ConcertAnalyseGenre = (props) => {
  const { genres } = props;
  return (
    <Fragment>
      <Header as="h3">
        Genres
      </Header>
      <ResponsiveContainer width="100%" height={190}>
        <PieChart>
          <Pie isAnimationActive={false} dataKey="value" data={genres} cx={90} cy={70} outerRadius={45} label>
            {
              genres.map(entry => (
                <Cell key={`cell-${entry.name}`} fill={entry.color || '#8884d8'} />
              ))
            }
          </Pie>
          <Legend verticalAlign="bottom" align="left" />
          <Tooltip />
        </PieChart>
      </ResponsiveContainer>
    </Fragment>);
};

ConcertAnalyseGenre.propTypes = {
  genres: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
  })).isRequired,
};

export default ConcertAnalyseGenre;
