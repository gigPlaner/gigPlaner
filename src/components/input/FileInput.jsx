import React, { Fragment } from 'react';
import firebase from 'firebase/app';
import 'firebase/storage';
import { Label, Icon } from 'semantic-ui-react';
import FileUploader from 'react-firebase-file-uploader';
import PropTypes from 'prop-types';

class FileInput extends React.Component {
  state = {
    isUploading: false,
    uploadDone: false,
    uploadError: false,
    msg: '',
    progress: 0,
  };

  handleUploadStart = () => {
    this.setState({
      isUploading: true,
      uploadDone: false,
      uploadError: false,
      progress: 0,
    });
  };

  handleUploadError = (err) => {
    this.setState({
      isUploading: false,
      uploadError: true,
      msg: err,
    });
  };

  handleUploadSuccess = (filename) => {
    this.setState({
      progress: 100,
      uploadDone: true,
      isUploading: false,
    });
    firebase
      .storage()
      .ref('notes')
      .child(filename)
      .getDownloadURL()
      .then(url => this.props.uploadSuccess(url));
  };

  handleProgress = (progress) => {
    this.setState({ progress });
  };

  render() {
    return (
      <Fragment>
        {/* little hack to display a button instead of the standard file input */}
        {/* eslint-disable-next-line */ }
        <label
          htmlFor="file"
          className="ui button no-after"
          style={{ display: 'inline-block' }}
          role="button"
        >
          <Icon name="upload" />
          Notenblatt auswählen
          <FileUploader
            id="file"
            hidden
            accept="image/*"
            randomizeFilename
            storageRef={firebase.storage()
              .ref('notes')}
            onUploadStart={this.handleUploadStart}
            onUploadError={this.handleUploadError}
            onUploadSuccess={this.handleUploadSuccess}
            onProgress={this.handleProgress}
          />
        </label>

        {this.state.isUploading && (
          <Label>
            Fortschritt:
            {this.state.progress}
            %
          </Label>
        )}
        {this.state.uploadDone
        && <Label>Upload abgeschlossen</Label>
        }
        {this.props.value
        && (
          <a href={this.props.value} target="_blank" rel="noreferrer noopener">  Notenblatt anzeigen</a>
        )}
        {this.state.uploadError && (
          <Label style={{ background: 'red' }}>
            Fehler:
            {this.state.msg}
          </Label>
        )}
      </Fragment>
    );
  }
}

FileInput.propTypes = {
  uploadSuccess: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
};

export default FileInput;
