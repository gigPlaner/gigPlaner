const GENRE = [
  {
    id: 1,
    name: 'rock',
    color: '#cc0000',
  },
  {
    id: 2,
    name: 'hip hop',
    color: '#1e23cc',
  },
  {
    id: 3,
    name: 'reggae',
    color: '#3fcc1f',
  },
  {
    id: 4,
    name: 'pop',
    color: '#bd54cc',
  },
  {
    id: 5,
    name: 'country',
    color: '#ccc528',
  },
];

export default GENRE;
