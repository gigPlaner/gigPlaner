import React, { Fragment } from 'react';
import {
  Button,
  Container,
  Dimmer,
  Divider,
  Grid,
  Header,
  Label,
  Loader,
  Segment,
  Icon, Message,
} from 'semantic-ui-react';
import ReactRouterPropTypes from 'react-router-prop-types';
import {
  convertSecondsToTimeString,
  toLocaleDateString,
} from '../../common/formHelper';
import SongList from './SongList';
import connectToStore from '../../common/storeUtil';
import { ConcertStore, SongStore } from '../../common/Store';
import { ConcertStoreType, SongStoreType } from '../../constants/types';

class ShowConcertScreen extends React.Component {
  state = {
    error: '',
  };

  async componentDidMount() {
    if (this.props.match.params.id) {
      const concert = await this.props.concertstore.getConcert(this.props.match.params.id);
      this.props.songstore.fetchSongs();
      if (!concert) {
        this.displayError(
          `Fehler beim Laden vom Konzert! Konzert mit ID ${this.props.match.params.id} existiert nicht.`,
        );
      }
    }
  }

  getSelectedSongs = (concert) => {
    const songs = this.props.songstore.state.songs;
    if (concert.songs.length < 1 || songs.length < 1) {
      return [];
    }
    const songValues = Object.values(songs);
    return concert.songs.map(song => songValues.find(s => s.id === song));
  };

  displayError = (error) => {
    this.setState({
      error,
    });
  };

  render() {
    const concert = this.props.concertstore.state.concerts
      .find(c => c.id === this.props.match.params.id);
    const { error: concertError, initialized: concertInitialized } = this.props.concertstore.state;
    const { error: songError, initialized: songInitialized } = this.props.songstore.state;
    const hasErrors = !!concertError || !!songError || !!this.state.error;
    const loading = (!concertInitialized || !songInitialized) && !hasErrors;

    return (
      <Fragment>
        <Header as="h1">
          Konzertübersicht
        </Header>
        <Dimmer inverted active={loading}>
          <Loader />
        </Dimmer>
        {!loading && !hasErrors
        && (
          <Grid divided="vertically">
            <Grid.Row columns={4}>
              <Grid.Column>
                <b>Datum</b>
              </Grid.Column>
              <Grid.Column>
                {toLocaleDateString(concert.date)}
              </Grid.Column>
              <Grid.Column>
                <b>Name</b>
              </Grid.Column>
              <Grid.Column>
                {concert.name}
              </Grid.Column>
              <Grid.Column>
                <b>Dauer maximal</b>
              </Grid.Column>
              <Grid.Column>
                {convertSecondsToTimeString(concert.maxDuration)}
              </Grid.Column>
              <Grid.Column>
                <b>Dauer aktuell</b>
              </Grid.Column>
              <Grid.Column>
                {convertSecondsToTimeString(concert.actualDuration)}
              </Grid.Column>
              <Grid.Column>
                <b>Ort</b>
              </Grid.Column>
              <Grid.Column width={9}>
                {concert.location && (
                  <Icon name="map marker alternate" link onClick={() => window.open(`https://www.google.com/maps/search/?api=1&query=${concert.location}`)} size="large" />
                )}
                {concert.location}
              </Grid.Column>
            </Grid.Row>
            {concert.description
            && (
              <Grid.Row columns={1}>
                <Grid.Column>
                  <Segment>
                    <Label attached="top left">Bemerkungen</Label>
                    <Container fluid>
                      {concert.description}
                    </Container>
                  </Segment>
                </Grid.Column>
              </Grid.Row>
            )}
            <Grid.Row columns={1}>
              <Grid.Column>
                <Segment>
                  <Label attached="top left">
                    Ausgewählte Songs
                  </Label>
                  <SongList songs={this.getSelectedSongs(concert)} />
                </Segment>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        ) }
        {hasErrors
        && (
          <Message
            error
            header="Fehler"
            list={[this.state.error, concertError, songError]}
          />
        ) }
        <Divider />
        <Button
          negative
          onClick={() => (this.props.history.push(
            '/concerts',
          ))}
        >
          Abbrechen
        </Button>
      </Fragment>
    );
  }
}

ShowConcertScreen.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  history: ReactRouterPropTypes.history.isRequired,
  concertstore: ConcertStoreType.isRequired,
  songstore: SongStoreType.isRequired,
};

export default connectToStore(
  {
    concertstore: ConcertStore,
    songstore: SongStore,
  },
)(ShowConcertScreen);
