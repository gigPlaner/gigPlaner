import PropTypes from 'prop-types';

export const GenreType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  genre: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
});

export const GenreStoreType = PropTypes.shape({
  genres: PropTypes.arrayOf(GenreType),
  error: PropTypes.string,
  loading: PropTypes.bool,
  initialized: PropTypes.bool,
  updateGenres: PropTypes.func.isRequired,
  fetchGenre: PropTypes.func.isRequired,
  getGenre: PropTypes.func.isRequired,
});

export const SongType = PropTypes.shape({
  id: PropTypes.string,
  title: PropTypes.string.isRequired,
  genre: PropTypes.oneOfType([PropTypes.string, GenreType]).isRequired,
  description: PropTypes.string,
  duration: PropTypes.number.isRequired,
  bpm: PropTypes.number.isRequired,
  active: PropTypes.bool.isRequired,
  fileURL: PropTypes.string.isRequired,
  selected: PropTypes.bool,
});

export const SongStoreType = PropTypes.shape({
  songs: PropTypes.arrayOf(SongType),
  error: PropTypes.string,
  loading: PropTypes.bool,
  initialized: PropTypes.bool,
  updateSongs: PropTypes.func.isRequired,
  fetchSongs: PropTypes.func.isRequired,
  getSong: PropTypes.func.isRequired,
});

export const ConcertType = PropTypes.shape({
  id: PropTypes.string,
  date: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  maxDuration: PropTypes.number.isRequired,
  actualDuration: PropTypes.number,
  description: PropTypes.string,
  songs: PropTypes.array,
});

export const ConcertFormType = PropTypes.shape({
  id: PropTypes.string,
  date: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  maxDuration: PropTypes.string.isRequired,
  actualDuration: PropTypes.number,
  description: PropTypes.string,
  songs: PropTypes.array,
});

export const ConcertStoreType = PropTypes.shape({
  concerts: PropTypes.arrayOf(ConcertType),
  error: PropTypes.string,
  loading: PropTypes.bool,
  initialized: PropTypes.bool,
  updateConcerts: PropTypes.func.isRequired,
  fetchConcerts: PropTypes.func.isRequired,
  getConcert: PropTypes.func.isRequired,
});
