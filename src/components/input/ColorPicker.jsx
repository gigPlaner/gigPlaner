import React, { Fragment } from 'react';
import { Button } from 'semantic-ui-react';
import { GithubPicker } from 'react-color';
import PropTypes from 'prop-types';

class ColorPicker extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      displayColorPicker: false,
    };
  }

  handleColorChange = ({ hex }) => {
    this.props.handleColorChange(hex);
  };

  handleClick = () => {
    this.setState(oldState => ({ displayColorPicker: !oldState.displayColorPicker }));
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false });
  };

  render() {
    const popover = {
      position: 'absolute',
      zIndex: '2',
    };

    const cover = {
      position: 'fixed',
      top: '0px',
      right: '0px',
      bottom: '0px',
      left: '0px',
    };

    return (
      <Fragment>
        <Button onClick={this.handleClick} style={{ background: this.props.value }}>
          Farbe auswählen
        </Button>
        {this.state.displayColorPicker ? (
          <div style={popover}>
            <div style={cover} onClick={this.handleClose} role="presentation" />
            <GithubPicker
              onChangeComplete={this.handleColorChange}
              onChange={this.handleClose}
              color={this.props.value}
            />
          </div>
        ) : null}
      </Fragment>
    );
  }
}

ColorPicker.defaultProps = {
  value: '',
};

ColorPicker.propTypes = {
  handleColorChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

export default ColorPicker;
