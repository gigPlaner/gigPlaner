const OfflineStatusEnum = (() => {
  const screens = {
    NotAvailable: 0, Available: 1, Loading: 2, Error: 3,
  };
  Object.freeze(screens);
  return screens;
})();

export default OfflineStatusEnum;
