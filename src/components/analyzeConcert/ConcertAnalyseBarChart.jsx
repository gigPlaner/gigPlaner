import React, { Fragment } from 'react';
import { Header } from 'semantic-ui-react';
import {
  Bar, BarChart, CartesianGrid, Cell, ResponsiveContainer, Tooltip, XAxis, YAxis,
} from 'recharts';
import PropTypes from 'prop-types';
import BarChartVerticalAxisTick from './BarChartVerticalAxisTick';
import BarChartCustomTooltip from './BarChartCustomTooltip';
import { SongType } from '../../constants/types';

const ConcertAnalyseBarChart = (props) => {
  const {
    songs, title, dataKey, isTimeValue,
  } = props;

  return (
    <Fragment>
      <Header as="h3">
        {title}
      </Header>
      <ResponsiveContainer width="100%" height={170}>
        <BarChart
          data={songs}
          margin={{
            top: 0, right: 0, left: 0, bottom: 50,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="title" tick={<BarChartVerticalAxisTick />} interval={0} />
          <YAxis />
          <Tooltip content={(
            <BarChartCustomTooltip
              labelName={title}
              formatLabelAsTimeString={isTimeValue}
            />
          )}
          />
          <Bar dataKey={dataKey} barSize={30}>
            {
              songs.map(entry => (
                <Cell key={`cell-${entry.title}`} fill={entry.genre ? entry.genre.color : '#8884d8'} />
              ))
            }
          </Bar>
        </BarChart>
      </ResponsiveContainer>
    </Fragment>);
};

ConcertAnalyseBarChart.propTypes = {
  songs: PropTypes.arrayOf(SongType).isRequired,
  title: PropTypes.string.isRequired,
  dataKey: PropTypes.string.isRequired,
  isTimeValue: PropTypes.bool,
};

ConcertAnalyseBarChart.defaultProps = {
  isTimeValue: false,
};

export default ConcertAnalyseBarChart;
