import React from 'react';
import { Grid } from 'semantic-ui-react';

import PropTypes from 'prop-types';
import ConcertAnalyseGenre from './ConcertAnalyseGenre';
import ConcertAnalyseInformation from './ConcertAnalyseInformation';
import ConcertAnalyseBarChart from './ConcertAnalyseBarChart';
import { ConcertFormType, SongType } from '../../constants/types';

class ConcertAnalyse extends React.Component {
  getGenresDistribution = (songs) => {
    const result = [];
    songs.reduce((res, value) => {
      if (!res[value.genre.id]) {
        res[value.genre.id] = {
          name: value.genre.genre,
          value: 0,
          color: value.genre.color,
        };
        result.push(res[value.genre.id]);
      }
      res[value.genre.id].value += 1;
      return res;
    }, {});
    return result;
  };

  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <ConcertAnalyseBarChart songs={this.props.songs} title="BPM" dataKey="bpm" />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <ConcertAnalyseBarChart songs={this.props.songs} title="Länge" dataKey="duration" isTimeValue />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={8} computer={8} style={{ marginBottom: '10px' }}>
            <ConcertAnalyseGenre genres={this.getGenresDistribution(this.props.songs)} />
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <ConcertAnalyseInformation songs={this.props.songs} concert={this.props.concert} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default ConcertAnalyse;

ConcertAnalyse.propTypes = {
  concert: ConcertFormType.isRequired,
  songs: PropTypes.arrayOf(SongType).isRequired,
};
