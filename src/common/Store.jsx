import { Container } from 'unstated/lib/unstated';
import { sortGenreByName, sortSongByName, sortConcertByDate } from './sortHelper';
import * as firestore from '../services/firestore';

export class GenreStore extends Container {
  state = {
    genres: [],
    error: null,
    loading: false,
    initialized: false,
  };

  updateGenres = (genres) => {
    this.setState({
      genres: genres.sort(sortGenreByName),
    });
  };

  fetchGenre = async () => {
    if (this.state.initialized
      || this.state.error || this.state.loading) {
      return;
    }
    await this.setState({ loading: true });
    try {
      const genres = await firestore.loadGenres();
      this.setState({
        genres: genres.sort(sortGenreByName),
        loading: false,
        initialized: true,
      });
      firestore.addGenreChangeListener(this.updateGenres);
    } catch (error) {
      const errorId = await firestore.logError(error);
      await this.setState({
        error: `Genres konnten nicht geladen werden! Error-Id: ${errorId || '-'}`,
        loading: false,
      });
    }
  };

  getGenre = async (id) => {
    await this.fetchGenre();
    return this.state.genres.find(genre => genre.id === id);
  };
}

export class SongStore extends Container {
  state = {
    songs: [],
    error: null,
    loading: false,
    initialized: false,
  };

  updateSongs = (songs) => {
    this.setState({
      songs: songs.sort(sortSongByName),
    });
  };

  fetchSongs = async () => {
    if (this.state.initialized
      || this.state.error || this.state.loading) {
      return;
    }
    await this.setState({ loading: true });
    try {
      const songs = await firestore.loadSongs();
      this.setState({
        songs: songs.sort(sortSongByName),
        loading: false,
        initialized: true,
      });
      firestore.addSongChangeListener(this.updateSongs);
    } catch (error) {
      const errorId = await firestore.logError(error);
      await this.setState({
        error: `Songs konnten nicht geladen werden! Error-Id: ${errorId || '-'}`,
        loading: false,
      });
    }
  };

  getSong = async (id) => {
    await this.fetchSongs();
    return this.state.songs.find(song => song.id === id);
  };
}

export class ConcertStore extends Container {
  state = {
    concerts: [],
    error: null,
    loading: false,
    initialized: false,
  };

  updateConcerts = (concerts) => {
    this.setState({
      concerts: concerts.sort(sortConcertByDate),
    });
  };

  fetchConcerts = async () => {
    if (this.state.initialized
      || this.state.error || this.state.loading) {
      return;
    }

    await this.setState({ loading: true });
    try {
      const concerts = await firestore.loadConcerts();
      this.setState({
        concerts: concerts.sort(sortConcertByDate),
        loading: false,
        initialized: true,
      });
      firestore.addConcertChangeListener(this.updateConcerts);
    } catch (error) {
      const errorId = await firestore.logError(error);
      await this.setState({
        error: `Konzerte konnten nicht geladen werden! Error-Id: ${errorId || '-'}`,
        loading: false,
      });
    }
  };

  getConcert = async (id) => {
    await this.fetchConcerts();
    return this.state.concerts.find(concert => concert.id === id);
  };
}
