import React from 'react';
import {
  Table,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import GenreRow from './GenreRow';
import { GenreType } from '../../constants/types';
import withLoader from '../LoadingComponent';

const GenreTable = props => (
  <Table celled padded>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          Genre
        </Table.HeaderCell>
        <Table.HeaderCell>
          Farbe
        </Table.HeaderCell>
        <Table.HeaderCell>
          Aktionen
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {props.genres.map(g => (
        <GenreRow genre={g} key={g.id} />
      ))}
    </Table.Body>
  </Table>
);

GenreTable.propTypes = {
  genres: PropTypes.arrayOf(GenreType).isRequired,
};

export default withLoader(GenreTable);
