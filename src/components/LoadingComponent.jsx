import React, { Fragment } from 'react';
import { Loader } from 'semantic-ui-react';

export default function withLoader(Component) {
  return (props) => {
    const { loading, ...passThroughProps } = props;
    return (
      <Fragment>
        {!loading ? (
          <Component {...passThroughProps} />
        ) : (
          <Loader active={loading} inline="centered" />
        )}
      </Fragment>
    );
  };
}
