import React from 'react';
import { Container, Icon, Menu } from 'semantic-ui-react';
import {
  Switch, BrowserRouter, Route, NavLink, Redirect,
} from 'react-router-dom';

import ROUTES from './constants/routes';
import ErrorBoundary from './components/ErrorBoundary';

const App = () => (
  <BrowserRouter>
    <div
      style={{
        // see some additional required styles in index.css
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
      }}
    >
      <div
        style={{
          flexGrow: 1,
          height: '100%',
          overflowX: 'hidden',
          overflowY: 'auto',
        }}
      >
        <Container
          style={{
            padding: '2em',
            height: '100%',
          }}
        >
          <Switch>
            {ROUTES.map((r, i) => (
              <Route
                path={r.url}
                // eslint-disable-next-line react/no-array-index-key
                key={i}
                exact={r.exact}
                render={props => (
                  <ErrorBoundary>{React.createElement(r.component, props)}</ErrorBoundary>
                )}
              />
            ))}
            <Redirect from="/index.html" to="/" />
          </Switch>
        </Container>
      </div>
      <Menu
        icon="labeled"
        borderless
        widths={4}
        style={{
          flexShrink: 0, // don't allow flexbox to shrink it
          borderRadius: 0, // clear semantic-ui style
          margin: 0, // clear semantic-ui style
        }}
        size="mini"
      >
        <Menu.Item as={NavLink} to="/concerts">
          <Icon name="play" />
            Konzerte
        </Menu.Item>
        <Menu.Item as={NavLink} to="/offline">
          <Icon name="cloud download" />
            Offline
        </Menu.Item>
        <Menu.Item as={NavLink} to="/songs">
          <Icon name="music" />
            Songs
        </Menu.Item>
        <Menu.Item as={NavLink} to="/genres">
          <Icon name="magic" />
            Genre
        </Menu.Item>
      </Menu>
    </div>
  </BrowserRouter>
);

export default App;
