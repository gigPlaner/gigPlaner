import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import {
  Button,
  Icon,
  List,
  Ref,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';
import { convertSecondsToShortTimeString } from '../../common/formHelper';
import { SongType } from '../../constants/types';

const DragAndDropSongList = (props) => {
  const onDragEnd = (result) => {
    const { destination, source } = result;
    if (!destination) {
      return;
    }
    if (destination.index === source.index) {
      return;
    }
    props.moveSong(source.index, destination.index);
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="droppableId">
        {provided => (
          <Ref innerRef={provided.innerRef}>
            <List
              divided
              style={
                {
                  height: props.maxHeight,
                  overflow: 'auto',
                }
              }
            >
              {props.songs.map((song, index) => (
                <Draggable
                  key={song.id}
                  draggableId={`song${song.id}`}
                  index={index}
                >
                  {providedDraggable => (
                    <Ref
                      innerRef={providedDraggable.innerRef}
                      key={song.id}
                    >
                      <List.Item
                        {...providedDraggable.draggableProps}
                      >
                        <List.Content floated="right">
                          <Button icon onClick={() => props.removeSong(song)}>
                            <Icon name="delete" />
                          </Button>
                        </List.Content>
                        <List.Icon
                          name="bars"
                          size="large"
                          {...providedDraggable.dragHandleProps}
                        />
                        <List.Content>
                          <List.Header>{song.title}</List.Header>
                          {`Dauer:  ${convertSecondsToShortTimeString(
                            song.duration,
                          )} / BPM: ${song.bpm}`}
                        </List.Content>
                      </List.Item>
                    </Ref>
                  )}
                </Draggable>
              ))}
            </List>
          </Ref>
        )}
      </Droppable>
    </DragDropContext>
  );
};

DragAndDropSongList.defaultProps = {
  maxHeight: '400px',
};

DragAndDropSongList.propTypes = {
  songs: PropTypes.arrayOf(SongType).isRequired,
  removeSong: PropTypes.func.isRequired,
  moveSong: PropTypes.func.isRequired,
  maxHeight: PropTypes.string,
};

export default DragAndDropSongList;
