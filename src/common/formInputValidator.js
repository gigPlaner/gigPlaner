const isRequired = 'isRequired';
const isNumber = 'isNumber';
const isDate = 'isDate';
const isTime = 'isTime';
const pattern = 'pattern';

export class FormInputRules {
  static isRequired() {
    this.requiredValue = true;
    return { [isRequired]: this.requiredValue };
  }

  static isNumber() {
    this.numberValue = true;
    return { [isNumber]: this.numberValue };
  }

  static pattern(patternValue = '') {
    this.patternValue = patternValue;
    return { [pattern]: this.patternValue };
  }

  static isDate() {
    this.dateValue = true;
    return { [isDate]: this.dateValue };
  }

  static isTime() {
    this.timeValue = true;
    return { [isTime]: this.timeValue };
  }
}

export class FormInputValidator {
  static validate(value, rules) {
    let isValueValid = true;

    rules.forEach((rule) => {
      const ruleMethod = Object.keys(rule)[0];
      const ruleParam = rule[ruleMethod];

      switch (ruleMethod) {
      case isRequired:
        isValueValid = isValueValid && this.isRequired(value);
        break;
      case isNumber:
        isValueValid = isValueValid && this.isNumber(value);
        break;
      case pattern:
        isValueValid = isValueValid && this.pattern(value, ruleParam);
        break;
      case isDate:
        isValueValid = isValueValid && this.isDate(value, ruleParam);
        break;
      case isTime:
        isValueValid = isValueValid && this.isTime(value, ruleParam);
        break;
      default:
        isValueValid = false;
      }
    });
    return isValueValid;
  }

  static isRequired(value) {
    if (value === undefined || value === null) {
      return false;
    }

    if (typeof value === 'string') {
      return value.trim() !== '';
    }
    return true;
  }

  static isNumber(value) {
    if (value === undefined || value === null || value === '') {
      return true;
    }
    if (typeof value === 'number') {
      return true;
    }

    return !Number.isNaN(Number(value));
  }

  static pattern(value, regExPattern) {
    if (value === undefined || value === null || value === '') {
      return true;
    }

    const regex = RegExp(regExPattern);
    return regex.test(value);
  }

  static isDate(value) {
    return this.pattern(value, '(\\d{4})-(\\d{2})-(\\d{2})');
  }

  static isTime(value) {
    return this.pattern(value, '^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])$');
  }
}
