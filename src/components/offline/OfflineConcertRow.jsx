import React from 'react';
import { Table, Icon, Button } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { convertSecondsToTimeString, toLocaleDateString } from '../../common/formHelper';
import { ConcertType } from '../../constants/types';

const OfflineConcertRow = props => (
  <Table.Row>
    <Table.Cell>
      {toLocaleDateString(props.concert.date)}
    </Table.Cell>
    <Table.Cell>
      {props.concert.name}
    </Table.Cell>
    <Table.Cell>
      {props.concert.location}
    </Table.Cell>
    <Table.Cell>
      {convertSecondsToTimeString(props.concert.actualDuration)}
    </Table.Cell>
    <Table.Cell>
      {props.concert.songs.length}
    </Table.Cell>
    <Table.Cell>
      <NavLink to={`/offline/play/${props.concert.id}`}>
        <Button icon>
          <Icon name="play" />
        </Button>
      </NavLink>
      <Button icon negative onClick={() => props.onRemove(props.concert)}>
        <Icon name="delete" />
      </Button>
    </Table.Cell>
  </Table.Row>
);

OfflineConcertRow.propTypes = {
  onRemove: PropTypes.func.isRequired,
  concert: ConcertType.isRequired,
};

export default OfflineConcertRow;
