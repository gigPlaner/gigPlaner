import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'unstated';
import App from './App';
import ErrorBoundary from './components/ErrorBoundary';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <ErrorBoundary>
      <Provider>
        <App />
      </Provider>
    </ErrorBoundary>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});
