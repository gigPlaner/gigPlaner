function sort(a, b) {
  let comparison = 0;
  if (a > b) {
    comparison = 1;
  } else if (a < b) {
    comparison = -1;
  }
  return comparison;
}

export function sortGenreByName(a, b) {
  // case insensitive sorting
  const genreA = a.genre.toLowerCase();
  const genreB = b.genre.toLowerCase();

  return sort(genreA, genreB);
}

export function sortSongByName(a, b) {
  // case insensitive sorting
  const songA = a.title.toLowerCase();
  const songB = b.title.toLowerCase();

  return sort(songA, songB);
}

export function sortConcertByDate(a, b) {
  const concertA = a.date;
  const concertB = b.date;

  return sort(concertA, concertB);
}
