import React from 'react';
import { Table, Button, Icon } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ConcertType } from '../../constants/types';
import { convertSecondsToTimeString, toLocaleDateString } from '../../common/formHelper';
import OfflineStatusEnum from '../../constants/offlineStatusEnum';

const ConcertRow = props => (
  <Table.Row>
    <Table.Cell>
      {toLocaleDateString(props.concert.date)}
    </Table.Cell>
    <Table.Cell>
      {props.concert.name}
    </Table.Cell>
    <Table.Cell>
      {props.concert.location}
    </Table.Cell>
    <Table.Cell>
      {convertSecondsToTimeString(props.concert.maxDuration)}
    </Table.Cell>
    <Table.Cell>
      {convertSecondsToTimeString(props.concert.actualDuration)}
    </Table.Cell>
    <Table.Cell>
      <NavLink to={`/concerts/show/${props.concert.id}`}>
        <Button icon>
          <Icon name="file outline" />
        </Button>
      </NavLink>
      <NavLink to={`/concerts/edit/${props.concert.id}`}>
        <Button icon>
          <Icon name="edit" />
        </Button>
      </NavLink>
      <Button
        icon
        onClick={() => props.downloadConcert(props.concert.id)}
        disabled={props.offlineConcertState === OfflineStatusEnum.Available}
        loading={props.offlineConcertState === OfflineStatusEnum.Loading}
        negative={props.offlineConcertState === OfflineStatusEnum.Error}
      >
        <Icon name="download" />
      </Button>
    </Table.Cell>
  </Table.Row>
);

ConcertRow.propTypes = {
  concert: ConcertType.isRequired,
  downloadConcert: PropTypes.func.isRequired,
  offlineConcertState: PropTypes.number,
};

ConcertRow.defaultProps = {
  offlineConcertState: OfflineStatusEnum.NotAvailable,
};

export default ConcertRow;
