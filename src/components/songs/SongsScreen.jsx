import React, { Fragment } from 'react';
import {
  Header, Icon, Button, Message,
} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import SongTable from './SongTable';
import connectToStore from '../../common/storeUtil';
import { GenreStore, SongStore } from '../../common/Store';
import { GenreStoreType, SongStoreType } from '../../constants/types';

class SongsScreen extends React.Component {
  componentDidMount() {
    this.props.songstore.fetchSongs();
    this.props.genrestore.fetchGenre();
  }

  render() {
    const { songs, error: songError, initialized: songInitialized } = this.props.songstore.state;
    const { error: genreError, initialized: genreInitialized } = this.props.genrestore.state;
    const hasErrors = !!songError || !!genreError;
    const loading = (!genreInitialized || !songInitialized) && !hasErrors;

    return (
      <Fragment>
        <Header as="h1">
          Songs
        </Header>
        <NavLink to="/songs/new">
          <Button
            icon
            labelPosition="left"
          >
            <Icon name="add" />
            Erfassen
          </Button>
        </NavLink>
        <SongTable songs={songs} loading={loading} />
        {hasErrors
          && (
            <Message
              error
              header="Fehler"
              list={[songError, genreError]}
            />
          )
        }
      </Fragment>
    );
  }
}

SongsScreen.propTypes = {
  songstore: SongStoreType.isRequired,
  genrestore: GenreStoreType.isRequired,
};

export default connectToStore({ songstore: SongStore, genrestore: GenreStore })(SongsScreen);
