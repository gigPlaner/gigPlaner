[![pipeline status](https://gitlab.com/gigPlaner/gigPlaner-prod/badges/master/pipeline.svg)](https://gitlab.com/gigPlaner/gigPlaner-prod/commits/master)
# GigPlaner
[https://gig-planer.firebaseapp.com](https://gig-planer.firebaseapp.com/)

## Branches
* master (protected)
* development
* ...

## Workflow
### lint
```
npm run lint
```

### lint & fix
```
npm run prettier
npm run lint:fix
```

### test
```
npm run test
```

## CI/CD
### CI
CI is run on each push to the repo.
Tests and the linter (eslint) are run for each branch.

### CD
Deployment is done only on changes in `master`.
