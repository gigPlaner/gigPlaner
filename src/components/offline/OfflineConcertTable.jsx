import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
} from 'semantic-ui-react';
import { ConcertType } from '../../constants/types';
import OfflineConcertRow from './OfflineConcertRow';

const ConcertTable = props => (
  <Table celled padded>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          Datum
        </Table.HeaderCell>
        <Table.HeaderCell>
          Konzertname
        </Table.HeaderCell>
        <Table.HeaderCell>
          Ort
        </Table.HeaderCell>
        <Table.HeaderCell>
          Dauer
        </Table.HeaderCell>
        <Table.HeaderCell>
          Anzahl Songs
        </Table.HeaderCell>
        <Table.HeaderCell>
          Aktionen
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {props.concerts.map(concert => (
        <OfflineConcertRow concert={concert} onRemove={props.onRemove} key={concert.id} />
      ))}
    </Table.Body>
  </Table>
);

ConcertTable.propTypes = {
  concerts: PropTypes.arrayOf(ConcertType).isRequired,
  onRemove: PropTypes.func.isRequired,
};

export default ConcertTable;
