import React, { Fragment } from 'react';
import {
  Header, Button, Icon, Message,
} from 'semantic-ui-react';
import {
  NavLink,
} from 'react-router-dom';
import ConcertTable from './ConcertTable';
import * as firestore from '../../services/firestore';
import * as offline from '../../services/localstorage';
import OfflineStatusEnum from '../../constants/offlineStatusEnum';
import { ConcertStore } from '../../common/Store';
import connectToStore from '../../common/storeUtil';
import { ConcertStoreType } from '../../constants/types';

class ConcertsScreen extends React.Component {
  state = {
    offlineConcertState: {},
    loadingOfflineSongs: true,
  };

  async componentDidMount() {
    const { concertstore } = this.props;
    await concertstore.fetchConcerts();

    try {
      const offlineConcertKeys = await offline.getConcertKeys();
      const reducer = (offlineConcertState, concert) => {
        if (offlineConcertKeys.includes(concert.id)) {
          return {
            ...offlineConcertState,
            [concert.id]: OfflineStatusEnum.Available,
          };
        }
        return offlineConcertState;
      };
      const offlineConcertState = concertstore.state.concerts.reduce(reducer, {});

      this.setState({
        offlineConcertState,
        loadingOfflineSongs: false,
      });
    } catch (error) {
      this.setState({
        loadingOfflineSongs: false,
      });
      firestore.logError(error);
    }
  }


  // mit store
  downloadConcert = async (id) => {
    this.setOfflineConcertStatus(id, OfflineStatusEnum.Loading);
    try {
      const concert = await firestore.getConcertWithSongs(id);
      if (concert) {
        await offline.storeConcert(concert);
        this.setOfflineConcertStatus(id, OfflineStatusEnum.Available);
      } else {
        this.setOfflineConcertStatus(id, OfflineStatusEnum.Error);
      }
    } catch (error) {
      this.setOfflineConcertStatus(id, OfflineStatusEnum.Error);
      firestore.logError(error);
    }
  };

  setOfflineConcertStatus = (concertId, status) => {
    this.setState(prevState => ({
      offlineConcertState: {
        ...prevState.offlineConcertState,
        [concertId]: status,
      },
    }));
  };

  render() {
    const { concerts, error, initialized } = this.props.concertstore.state;
    const loading = (!initialized && !error) || this.state.loadingOfflineSongs;
    return (
      <Fragment>
        <Header as="h1">
          Konzerte
        </Header>
        <NavLink to="/concerts/new">
          <Button
            icon
            labelPosition="left"
          >
            Erfassen
            <Icon name="add" />
          </Button>
        </NavLink>
        <ConcertTable
          concerts={concerts}
          offlineConcertState={this.state.offlineConcertState}
          downloadConcert={this.downloadConcert}
          loading={loading}
        />
        {error
          && (
            <Message
              error
              header="Fehler"
              content={error}
            />
          )
        }
      </Fragment>
    );
  }
}

ConcertsScreen.propTypes = {
  concertstore: ConcertStoreType.isRequired,
};

export default connectToStore({ concertstore: ConcertStore })(ConcertsScreen);
