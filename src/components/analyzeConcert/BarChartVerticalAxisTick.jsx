import React from 'react';
import PropTypes from 'prop-types';

const BarChartVerticalAxisTick = (props) => {
  const { payload, x, y } = props;
  let value = payload.value;
  if (value && value.length > 5) {
    value = `${value.slice(0, 5)}.`;
  }

  return (
    <g transform={`translate(${x},${y})`}>
      <text x={-5} y={0} dy={5} textAnchor="end" fill="#666" transform="rotate(-90)">{value}</text>
    </g>);
};

BarChartVerticalAxisTick.propTypes = {
  payload: PropTypes.shape({ value: PropTypes.string }),
  x: PropTypes.number,
  y: PropTypes.number,
};

BarChartVerticalAxisTick.defaultProps = {
  payload: { value: null },
  x: 0,
  y: 0,
};

export default BarChartVerticalAxisTick;
