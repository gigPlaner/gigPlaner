import {
  List,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import React from 'react';
import { convertSecondsToShortTimeString } from '../../common/formHelper';
import { SongType } from '../../constants/types';

const SongList = props => (
  <List
    divided
    style={
      {
        height: props.maxHeight,
        overflow: 'auto',
      }
    }
  >
    {props.songs.map(song => (
      <List.Item
        key={song.id}
      >
        <List.Content floated="right">
          {props.renderButtons(song)}
        </List.Content>

        <List.Content>
          <List.Header>{song.title}</List.Header>
          {`Dauer:  ${convertSecondsToShortTimeString(
            song.duration,
          )} / BPM: ${song.bpm}`}
        </List.Content>
      </List.Item>
    ))}
  </List>
);

SongList.defaultProps = {
  renderButtons: () => {},
  maxHeight: '400px',
};


SongList.propTypes = {
  songs: PropTypes.arrayOf(SongType).isRequired,
  renderButtons: PropTypes.func,
  maxHeight: PropTypes.string,
};

export default SongList;
