import {
  Button,
  Divider,
  Grid, Icon,
  Input,
  Label,
  Progress,
  Segment,
  Statistic,
} from 'semantic-ui-react';
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { SongType } from '../../constants/types';
import { convertSecondsToTimeString } from '../../common/formHelper';
import DragAndDropSongList from './DragAndDropSongList';
import SongList from './SongList';

export default class SongSelection extends Component {
  state = {
    searchString: '',
  };

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState(() => ({
      [name]: value,
    }));
  };

  getFilteredSongs = () => this.props.songRepository.filter(
    s => s.active === true && s.title.toUpperCase().indexOf(this.state.searchString.toUpperCase())
      > -1,
  );

  renderAddButton = song => (
    <Button
      icon
      onClick={() => this.props.addSong(song)}
      disabled={song.selected}
    >
      <Icon name="add" />
    </Button>
  );

  render() {
    const error = this.props.maxDuration < this.props.actualDuration;
    const percentTotalDuration = Math.round(
      (this.props.actualDuration / this.props.maxDuration) * 100,
    );

    return (
      <Fragment>
        <Grid divided="vertically">
          <Grid.Row columns={2}>
            <Grid.Column>
              <Input
                fluid
                icon="search"
                placeholder="Search..."
                name="searchString"
                value={this.state.searchString}
                onChange={this.handleInputChange}
              />
            </Grid.Column>
            <Grid.Column />
            <Grid.Column>
              <Segment>
                <Label attached="top left">
                  Song-Repo
                </Label>
                <SongList
                  songs={this.getFilteredSongs()}
                  renderButtons={this.renderAddButton}
                />
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>
                <Label attached="top left">
                  Ausgewählte Songs
                </Label>
                <DragAndDropSongList
                  songs={this.props.selectedSongs}
                  removeSong={this.props.removeSong}
                  moveSong={this.props.moveSong}
                />
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row columns={1}>
            <Grid.Column>
              <Statistic.Group size="mini">
                <Statistic size="mini">
                  <Statistic.Label>Geplante Dauer</Statistic.Label>
                  <Statistic.Value>
                    {convertSecondsToTimeString(this.props.maxDuration)}
                  </Statistic.Value>
                </Statistic>
                <Statistic size="mini">
                  <Statistic.Label>Tatsächliche Dauer</Statistic.Label>
                  <Statistic.Value>
                    {convertSecondsToTimeString(this.props.actualDuration)}
                  </Statistic.Value>
                </Statistic>
              </Statistic.Group>
              <Divider hidden />
              <Progress
                error={error}
                progress
                percent={percentTotalDuration}
              >
                {error === true ? 'Die aktuelle Dauer ist zu lange' : null}
              </Progress>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Fragment>
    );
  }
}

SongSelection.propTypes = {
  songRepository: PropTypes.arrayOf(SongType).isRequired,
  selectedSongs: PropTypes.arrayOf(SongType).isRequired,
  maxDuration: PropTypes.number.isRequired,
  actualDuration: PropTypes.number.isRequired,
  addSong: PropTypes.func.isRequired,
  removeSong: PropTypes.func.isRequired,
  moveSong: PropTypes.func.isRequired,
};
