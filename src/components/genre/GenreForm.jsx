import React, { Fragment } from 'react';
import {
  Form, Input, Button, Divider, Header, Message, Dimmer, Loader,
} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import ReactRouterPropTypes from 'react-router-prop-types';
import * as firestore from '../../services/firestore';
import ColorPicker from '../input/ColorPicker';
import { FormInputRules, FormInputValidator } from '../../common/formInputValidator';
import { GenreStore } from '../../common/Store';
import connectToStore from '../../common/storeUtil';
import { GenreStoreType } from '../../constants/types';

class GenreForm extends React.Component {
  formValidation = {
    genre: [FormInputRules.isRequired()],
    color: [FormInputRules.isRequired()],
  };

  state = {
    genre: '',
    color: '',
    formError: '',
    formErrors: {},
    formStoring: false,
  };

  async componentDidMount() {
    if (this.isEditMode()) {
      // load the genre form store and put it into our state
      const genre = await this.props.genrestore.getGenre(this.props.match.params.id);
      if (genre) {
        this.setState({
          ...genre,
        });
      } else {
        this.displayError(`Genre konnte nicht geladen werden. Genre mit ID ${this.props.match.params.id} existiert nicht.`);
      }
    }
  }

  isEditMode = () => this.props.match.params.id !== undefined;

  markInputFieldError = (input, isFieldValid) => {
    this.setState(prevState => ({
      formErrors: {
        ...prevState.formErrors,
        [input]: !isFieldValid,
      },
    }));
  };

  isValidInput = () => {
    let formValid = true; // assume form is valid

    Object.keys(this.formValidation).forEach(
      (inputField) => {
        const rules = this.formValidation[inputField];
        const value = this.state[inputField];
        const isFieldValid = FormInputValidator.validate(value, rules);
        formValid = formValid && isFieldValid;
        this.markInputFieldError(inputField, isFieldValid);
      },
    );

    if (!formValid) {
      this.displayError('Formular unvollständig: Nicht alle erforderlichen Felder sind ausgefüllt.');
    }

    return formValid;
  };

  displayError = (err) => {
    this.setState({
      formError: err,
      formStoring: false,
    });
  };

   handleSubmit = async () => {
     this.setState({
       formStoring: true,
     });

     if (!this.isValidInput()) {
       return;
     }

     const genre = {
       id: this.state.id,
       genre: this.state.genre,
       color: this.state.color,
     };

     if (this.isEditMode()) {
       try {
         // update
         await firestore.updateGenre(genre);
         this.props.history.push('/genres');
       } catch (error) {
         this.displayError('Genre konnte nicht aktualisiert werden.');
         firestore.logError(error);
       }
     } else {
       try {
         // add a new genre
         await firestore.storeGenre(genre);
         // redirect back to /genres
         this.props.history.push('/genres');
       } catch (error) {
         this.displayError('Genre konnte nicht gespeichert werden.');
         firestore.logError(error);
       }
     }
   };

  // handles changes of input field(s)
  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  };

  // handles changes of the color picker
  handleColorChange = (color) => {
    this.setState({
      color,
    });
  };

  render() {
    const { error: loadingError, initialized } = this.props.genrestore.state;
    const hasError = !!loadingError || !!this.state.formError;
    const loading = !initialized && !hasError;
    return (
      <Fragment>
        <Header as="h1">
          {this.isEditMode() ? 'Genre bearbeiten' : 'Neues Gerne erfassen'}
        </Header>
        <Dimmer inverted active={loading || this.state.formStoring}>
          <Loader />
        </Dimmer>
        {(!loading || !this.isEditMode())
              && (
                <Form
                  noValidate // tell the browser to not validate the form
                  autoComplete="off"
                  error={hasError}
                >

                  <Form.Field
                    label="Genre"
                    type="text"
                    id="genre"
                    name="genre"
                    control={Input}
                    onChange={this.handleInputChange}
                    value={this.state.genre}
                    autoFocus
                    required
                    error={this.state.formErrors.genre}
                  />
                  <Form.Field
                    label="Farbe"
                    id="color"
                    name="color"
                    control={ColorPicker}
                    handleColorChange={this.handleColorChange}
                    value={this.state.color}
                    required
                    error={this.state.formErrors.color}
                  />
                  <Message
                    error
                    header="Fehler"
                    list={[this.state.formError, loadingError]}
                  />
                  <Divider />
                  <Button
                    onClick={this.handleSubmit}
                  >
                    Speichern
                  </Button>

                  <NavLink to="/genres">
                    <Button
                      negative
                    >
                      Abbrechen
                    </Button>
                  </NavLink>
                </Form>
              )}
      </Fragment>
    );
  }
}

GenreForm.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  match: ReactRouterPropTypes.match.isRequired,
  genrestore: GenreStoreType.isRequired,
};

export default connectToStore({ genrestore: GenreStore })(GenreForm);
