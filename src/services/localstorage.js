import Dexie from 'dexie';

const indexedDB = new Dexie('concerts');

indexedDB.version(1)
  .stores({
    concert: 'id',
  });


const downloadMusicSheet = song => fetch(song.fileURL);

export const storeConcert = (concert) => {
  const promises = [];

  // download the music sheet for each song
  concert.songs.forEach((song) => {
    let type;
    promises.push(downloadMusicSheet(song)
      .then((resp) => {
        type = resp.headers.get('content-type');
        return resp.arrayBuffer();
      })
      .then((buffer) => {
        const newSong = song;
        newSong.file = { type, buffer };
      }));
  });

  return Promise.all(promises)
    .then(() => {
      indexedDB.concert.add({
        ...concert,
      });
    });
};

export const loadConcerts = () => indexedDB.concert.toArray();

export const getConcertKeys = () => indexedDB.concert.toCollection().primaryKeys();

export const getConcert = id => indexedDB.concert.get({ id });

export const deleteConcert = concert => indexedDB.concert.delete(concert.id);
