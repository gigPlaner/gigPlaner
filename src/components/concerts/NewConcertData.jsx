import React, { Fragment } from 'react';
import {
  Form, Input, TextArea,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { ConcertFormType } from '../../constants/types';

const NewConcertData = props => (
  <Fragment>
    <Form>
      <Form.Field
        label="Datum"
        id="date"
        name="date"
        type="date"
        required
        control={Input}
        autoFocus
        value={props.concert.date}
        onChange={props.handleInputChange}
      />
      <Form.Field
        label="Name"
        id="name"
        name="name"
        required
        control={Input}
        value={props.concert.name}
        onChange={props.handleInputChange}
      />
      <Form.Field
        label="Ort"
        id="location"
        name="location"
        control={Input}
        required
        value={props.concert.location}
        onChange={props.handleInputChange}
      />
      <Form.Field
        label="Dauer maximal"
        id="maxDuration"
        name="maxDuration"
        placeholder="hh:mm:ss"
        required
        control={Input}
        type="text"
        onBlur={props.validateInputField}
        onChange={props.handleInputChange}
        value={props.concert.maxDuration}
        error={props.formErrors.maxDuration}
      />
      <Form.Field
        label="Beschreibung"
        id="description"
        name="description"
        control={TextArea}
        type="text"
        rows="4"
        onChange={props.handleInputChange}
        value={props.concert.description}
      />
    </Form>
  </Fragment>
);

NewConcertData.propTypes = {
  concert: ConcertFormType.isRequired,
  formErrors: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  handleInputChange: PropTypes.func.isRequired,
  validateInputField: PropTypes.func.isRequired,
};

export default NewConcertData;
