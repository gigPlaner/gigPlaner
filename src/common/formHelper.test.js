import {
  convertSecondsToTimeString,
  convertTimeStringToSeconds,
  convertSecondsToShortTimeString,
  addTimeColon,
} from './formHelper';

test('test convert time to seconds', () => {
  expect(convertTimeStringToSeconds('')).toBe('');
  expect(convertTimeStringToSeconds('00:00:00')).toBe(0);
  expect(convertTimeStringToSeconds('00:00:01')).toBe(1);
  expect(convertTimeStringToSeconds('00:02:01')).toBe(121);
  expect(typeof convertTimeStringToSeconds('00:02:01') === 'number').toBeTruthy();
});

test('test convert seconds to time', () => {
  expect(convertSecondsToTimeString('')).toBe('');
  expect(convertSecondsToTimeString(0)).toBe('00:00:00');
  expect(convertSecondsToTimeString(1)).toBe('00:00:01');
  expect(convertSecondsToTimeString(121)).toBe('00:02:01');
});

test('test convert seconds to time (short)', () => {
  expect(convertSecondsToShortTimeString('')).toBe('');
  expect(convertSecondsToShortTimeString(0)).toBe('00:00');
  expect(convertSecondsToShortTimeString(1)).toBe('00:01');
  expect(convertSecondsToShortTimeString(121)).toBe('02:01');
  expect(convertSecondsToShortTimeString(3601)).toBe('01:00:01');
});

test('test add time colon', () => {
  expect(addTimeColon('', '')).toBe('');
  expect(addTimeColon('', '0')).toBe('0');
  expect(addTimeColon('0', '00')).toBe('00:');
  expect(addTimeColon('00:0', '00:00')).toBe('00:00:');
  expect(addTimeColon('00', '00:')).toBe('00:');
  expect(addTimeColon('00:', '00')).toBe('00');
  expect(addTimeColon('02:01:00', '02:01:001')).toBe('02:01:00');
  expect(addTimeColon('02:0', '02:0s')).toBe('02:0');
});
