import React, { Fragment } from 'react';
import {
  Button, Header, Icon, Message,
} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import GenreTable from './GenreTable';
import { GenreStore } from '../../common/Store';
import connectToStore from '../../common/storeUtil';
import { GenreStoreType } from '../../constants/types';

class GenresScreen extends React.Component {
  async componentDidMount() {
    this.props.genrestore.fetchGenre();
  }

  render() {
    const { genres, error, initialized } = this.props.genrestore.state;
    const loading = !initialized && !error;
    return (
      <Fragment>
        <Header as="h1">
        Genre
        </Header>
        <NavLink to="/genres/new">
          <Button
            icon
            labelPosition="left"
          >
            <Icon name="add" />
          Erfassen
          </Button>
        </NavLink>
        <GenreTable genres={genres} loading={loading} />
        {error
      && (
        <Message
          error
          header="Fehler"
          list={[error]}
        />
      )}
      </Fragment>
    );
  }
}

GenresScreen.propTypes = {
  genrestore: GenreStoreType.isRequired,
};

export default connectToStore({ genrestore: GenreStore })(GenresScreen);
