import {
  Table, Button, Icon, Segment,
} from 'semantic-ui-react';
import React from 'react';
import { NavLink } from 'react-router-dom';
import { GenreType } from '../../constants/types';

const GenreRow = props => (
  <Table.Row key={props.genre.id}>
    <Table.Cell>
      {props.genre.genre}
    </Table.Cell>
    <Table.Cell>
      <Segment style={{ backgroundColor: props.genre.color }} />
    </Table.Cell>
    <Table.Cell>
      <NavLink to={`/genres/edit/${props.genre.id}`}>
        <Button icon>
          <Icon name="edit" />
        </Button>
      </NavLink>
    </Table.Cell>
  </Table.Row>
);

GenreRow.propTypes = {
  genre: GenreType.isRequired,
};

export default GenreRow;
