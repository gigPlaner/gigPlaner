import React, { Fragment } from 'react';
import {
  Header, Image, Button, Message, Grid, Divider,
} from 'semantic-ui-react';
import ReactRouterPropTypes from 'react-router-prop-types';
import { getConcert } from '../../services/localstorage';

class PlayConcertScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      concert: {
        songs: [],
      },
      currentSong: {},
      imageURL: '',
      songIndex: 0,
      nextButtonDisabled: false,
      prevButtonDisabled: true,
    };
  }

  componentDidMount() {
    getConcert(this.props.match.params.id)
      .then((concert) => {
        this.setState({
          concert,
          currentSong: concert.songs[0],
          imageURL: this.getURL(concert.songs[0]),
        });
      })
      .catch(() => {
        this.displayError(`Konzert mit ID ${this.props.match.params.id} konnte nicht geladen werden!`);
      });
  }

  changeSong = (nextIdx) => {
    const nextSongIdx = this.state.songIndex + nextIdx;

    if (nextSongIdx < this.state.concert.songs.length && nextSongIdx >= 0) {
      window.URL.revokeObjectURL(this.state.imageURL); // revoke the old object url

      this.setState(state => ({
        currentSong: state.concert.songs[nextSongIdx],
        songIndex: nextSongIdx,
        imageURL: this.getURL(state.concert.songs[nextSongIdx]),
      }));

      if (nextSongIdx > 0) {
        this.setState({
          prevButtonDisabled: false,
        });
      } else {
        this.setState({
          prevButtonDisabled: true,
        });
      }

      if (nextSongIdx === this.state.concert.songs.length - 1) {
        this.setState({
          nextButtonDisabled: true,
        });
      } else {
        this.setState({
          nextButtonDisabled: false,
        });
      }
    }
  };

  arrayBufferToBlob = (buffer, type) => new Blob([buffer], { type });

  getURL = song => window.URL.createObjectURL(
    this.arrayBufferToBlob(song.file.buffer, song.file.type),
  );

  stopConcert = () => {
    this.props.history.push('/offline');
  };

  displayError = (err) => {
    this.setState({
      error: err,
      prevButtonDisabled: true,
      nextButtonDisabled: true,
    });
  };

  render() {
    const numSongs = this.state.concert.songs.length;
    const hasError = !!this.state.error;
    return (
      <Fragment>
        {hasError
          ? <Message header="Fehler!" content={this.state.error} error />
          : (
            <Fragment>
              <Header as="h1">
                {`${this.state.concert.name} (${this.state.songIndex + 1}/${numSongs}) - ${this.state.currentSong.title}`}
              </Header>
              <Image centered src={this.state.imageURL} style={{ maxHeight: '87%' }} />
            </Fragment>
          )
        }
        <Divider />
        <Grid
          centered
          columns="3"
        >
          <Grid.Column textAlign="left">
            <Button onClick={() => this.changeSong(-1)} disabled={this.state.prevButtonDisabled}>
              Zurück
            </Button>
          </Grid.Column>
          <Grid.Column textAlign="center">
            <Button negative onClick={() => this.stopConcert()}>
              Beenden
            </Button>
          </Grid.Column>
          <Grid.Column textAlign="right">
            <Button onClick={() => this.changeSong(1)} disabled={this.state.nextButtonDisabled}>
              Weiter
            </Button>
          </Grid.Column>
        </Grid>
      </Fragment>
    );
  }
}

PlayConcertScreen.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  history: ReactRouterPropTypes.history.isRequired,
};

export default PlayConcertScreen;
