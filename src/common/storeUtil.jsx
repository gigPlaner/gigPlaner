import React from 'react';
import { Subscribe } from 'unstated';

export default function connectToStore(stores) {
  return function subscribe(Component) {
    return props => (
      <Subscribe to={[...Object.values(stores)]}>
        {(...values) => {
          const storesObject = Object.keys(stores).reduce((acc, key, i) => {
            acc[key] = values[i];
            return acc;
          }, {});
          return <Component {...storesObject} {...props} />;
        }
        }
      </Subscribe>
    );
  };
}
