import ConcertsScreen from '../components/concerts/ConcertsScreen';
import SongsScreen from '../components/songs/SongsScreen';
import ConcertWizardScreen from '../components/concerts/ConcertWizardScreen';
import GenresScreen from '../components/genre/GenresScreen';
import GenreForm from '../components/genre/GenreForm';
import SongForm from '../components/songs/SongForm';
import OfflineConcert from '../components/offline/OfflineConcert';
import ShowConcertScreen from '../components/concerts/ShowConcertScreen';
import PlayConcertScreen from '../components/offline/PlayConcertScreen';

const ROUTES = [
  {
    url: '/',
    component: ConcertsScreen,
    exact: true,
  },
  {
    url: '/offline/play/:id',
    component: PlayConcertScreen,
    exact: false,
  },
  {
    url: '/offline',
    component: OfflineConcert,
    exact: false,
  },
  {
    url: '/concerts/new',
    component: ConcertWizardScreen,
    exact: false,
  },
  {
    url: '/concerts',
    component: ConcertsScreen,
    exact: true,
  },
  {
    url: '/concerts/edit/:id',
    component: ConcertWizardScreen,
    exact: false,
  },
  {
    url: '/concerts/show/:id',
    component: ShowConcertScreen,
    exact: false,
  },
  {
    url: '/songs/edit/:id',
    component: SongForm,
    exact: false,
  },
  {
    url: '/songs/new',
    component: SongForm,
    exact: false,
  },
  {
    url: '/songs',
    component: SongsScreen,
    exact: false,
  },
  {
    url: '/genres/edit/:id',
    component: GenreForm,
    exact: false,
  },
  {
    url: '/genres/new',
    component: GenreForm,
    exact: false,
  },
  {
    url: '/genres',
    component: GenresScreen,
    exact: false,
  },
];

export default ROUTES;
