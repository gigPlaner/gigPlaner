import React from 'react';
import {
  Table,
} from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { ConcertType } from '../../constants/types';
import ConcertRow from './ConcertRow';
import withLoader from '../LoadingComponent';

const ConcertTable = props => (
  <Table celled padded>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          Datum
        </Table.HeaderCell>
        <Table.HeaderCell>
          Konzertname
        </Table.HeaderCell>
        <Table.HeaderCell>
          Ort
        </Table.HeaderCell>
        <Table.HeaderCell>
          Dauer maximal
        </Table.HeaderCell>
        <Table.HeaderCell>
          Dauer aktuell
        </Table.HeaderCell>
        <Table.HeaderCell>
          Aktionen
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
    <Table.Body>
      {props.concerts.map(concert => (
        <ConcertRow
          concert={concert}
          key={concert.id}
          downloadConcert={props.downloadConcert}
          offlineConcertState={props.offlineConcertState[concert.id]}
        />
      ))}
    </Table.Body>
  </Table>
);

ConcertTable.propTypes = {
  concerts: PropTypes.arrayOf(ConcertType).isRequired,
  downloadConcert: PropTypes.func.isRequired,
  offlineConcertState: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default withLoader(ConcertTable);
