import { sortConcertByDate, sortGenreByName, sortSongByName } from './sortHelper';

test('sort genres by name', () => {
  const unsortedGenres = [
    { genre: 'Pop' },
    { genre: 'Rock' },
    { genre: 'Blues' },
  ];

  const expectedGenres = [
    { genre: 'Blues' },
    { genre: 'Pop' },
    { genre: 'Rock' },
  ];

  expect(unsortedGenres.sort(sortGenreByName))
    .toEqual(expectedGenres);
});

test('sort songs by name', () => {
  const unsortedSongs = [
    { title: 'Where the wild goes' },
    { title: 'Knocking on heavens door' },
    { title: 'Light up the sky' },
  ];

  const expetedSongs = [
    { title: 'Knocking on heavens door' },
    { title: 'Light up the sky' },
    { title: 'Where the wild goes' },
  ];

  expect(unsortedSongs.sort(sortSongByName))
    .toEqual(expetedSongs);
});

test('sort concerts by date', () => {
  const unsortedConcerts = [
    { date: '18.09.2018' },
    { date: '10.01.2001' },
    { date: '01.10.1993' },
  ];

  const expectedConcerts = [
    { date: '01.10.1993' },
    { date: '10.01.2001' },
    { date: '18.09.2018' },
  ];
  expect(unsortedConcerts.sort(sortConcertByDate))
    .toEqual(expectedConcerts);
});
