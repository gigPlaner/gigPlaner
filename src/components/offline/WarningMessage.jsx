import {
  Message,
} from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import React from 'react';

const WarningMessage = () => (
  <Message warning>
    <Message.Header>
      Kein Konzert verfügbar
    </Message.Header>
    {'Du kannst Konzerte '}
    <NavLink to="/concerts">
      hier
    </NavLink>
    {' herunterladen, damit diese Offline verfügbar werden.'}
  </Message>
);

export default WarningMessage;
