import { FormInputValidator, FormInputRules } from './formInputValidator';

test('test is required valid', () => {
  expect(FormInputValidator.validate('', [FormInputRules.isRequired()])).toBe(false);
  expect(FormInputValidator.validate(' ', [FormInputRules.isRequired()])).toBe(false);
  expect(FormInputValidator.validate(null, [FormInputRules.isRequired()])).toBe(false);

  expect(FormInputValidator.validate(0, [FormInputRules.isRequired()])).toBe(true);
  expect(FormInputValidator.validate('1', [FormInputRules.isRequired()])).toBe(true);
});

test('test is number valid', () => {
  expect(FormInputValidator.validate('asd', [FormInputRules.isNumber()])).toBe(false);

  expect(FormInputValidator.validate('', [FormInputRules.isNumber()])).toBe(true);
  expect(FormInputValidator.validate(null, [FormInputRules.isNumber()])).toBe(true);

  expect(FormInputValidator.validate('',
    [FormInputRules.isRequired(), FormInputRules.isNumber()])).toBe(false);
  expect(FormInputValidator.validate(null,
    [FormInputRules.isRequired(), FormInputRules.isNumber()])).toBe(false);

  expect(FormInputValidator.validate(0, [FormInputRules.isNumber()])).toBe(true);
  expect(FormInputValidator.validate('1', [FormInputRules.isNumber()])).toBe(true);
});

test('test is date valid', () => {
  expect(FormInputValidator.validate('1', [FormInputRules.isDate()])).toBe(false);
  expect(FormInputValidator.validate(0, [FormInputRules.isDate()])).toBe(false);
  expect(FormInputValidator.validate('2018-01', [FormInputRules.isDate()])).toBe(false);

  expect(FormInputValidator.validate('', [FormInputRules.isDate()])).toBe(true);
  expect(FormInputValidator.validate(null, [FormInputRules.isDate()])).toBe(true);

  expect(FormInputValidator.validate('',
    [FormInputRules.isRequired(), FormInputRules.isDate()])).toBe(false);
  expect(FormInputValidator.validate(null,
    [FormInputRules.isRequired(), FormInputRules.isDate()])).toBe(false);

  expect(FormInputValidator.validate('2018-01-02', [FormInputRules.isDate()])).toBe(true);
});

test('test is time valid', () => {
  expect(FormInputValidator.validate('1', [FormInputRules.isTime()])).toBe(false);
  expect(FormInputValidator.validate(0, [FormInputRules.isTime()])).toBe(false);
  expect(FormInputValidator.validate('10:00', [FormInputRules.isTime()])).toBe(false);

  expect(FormInputValidator.validate('', [FormInputRules.isTime()])).toBe(true);
  expect(FormInputValidator.validate(null, [FormInputRules.isTime()])).toBe(true);

  expect(FormInputValidator.validate('',
    [FormInputRules.isRequired(), FormInputRules.isTime()])).toBe(false);
  expect(FormInputValidator.validate(null,
    [FormInputRules.isRequired(), FormInputRules.isTime()])).toBe(false);

  expect(FormInputValidator.validate('10:00:14', [FormInputRules.isTime()])).toBe(true);
});
